#!/usr/bin/env python3

import os
import shutil

import daemon

from modules.config_arg import get_args
from modules.config_log import config_log
from modules.read_input import read_input
from modules.surface import run_surface


def solvhybrid():
    logger.info(f'SolvHybrid started on {os.getcwd()}.')
    logger.info(f'To kill SolvHybrid execution type: `$ kill {os.getpid()}`.')
    logger.info(f"Using '{args.input}' as input.")

    inp_vars = read_input(args.input)

    if inp_vars["surface"]:
        run_surface(inp_vars)

    if inp_vars["bulk"]:
        pass  # TODO Implement it

    shutil.move("leap.log", "tleap/")
    logger.info(f'SolvHybrid finished.')


args = get_args()
logger = config_log('SolvHybrid')

print("Running SolvHybrid.\n"
      f"To check SolvHybrid activity see '{logger.handlers[0].baseFilename}'.")

if args.foreground:
    solvhybrid()
else:
    with daemon.DaemonContext(working_directory=os.getcwd(), umask=0o002,
                              files_preserve=[
                                  logger.handlers[0].stream.fileno()]):
        # From here on, the execution is carried out by a separate process in
        # background
        solvhybrid()

def minimize(wd):
    """Does minimization with sander and process the trajectory with cpptraj.

    @param wd: The working directory.
    """
    from subprocess import Popen
    import os

    orig_dir = os.getcwd()
    os.chdir(wd)

    # Write input for and run sander.  # TODO customize solvent
    sander_inp_str = """Minimize
  &cntrl
   imin=1,
   ntx=1,
   irest=0,
   maxcyc=2000,
   ncyc=1000,
   ntpr=100,
   ntwx=0,
   cut=8.0,
   ntc=2,
   ibelly=1,
   bellymask=':WAT'

 /
 """
    with open("mdin", "w") as mdin_fh:
        mdin_fh.write(sander_inp_str)
    Popen("sander -O", shell=True).wait()

    # Write input for and run cpptraj.
    cpptraj_inp_file = """trajin restrt
    outtraj minimized.pdb lastframe
    """
    with open("cpptrajin", "w") as cpptrajin_fh:
        cpptrajin_fh.write(cpptraj_inp_file)
    Popen(f"cpptraj -p prmtop -i cpptrajin", shell=True).wait()

    os.chdir(orig_dir)

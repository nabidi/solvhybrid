import logging
import numpy as np

logger = logging.getLogger('SolvHybrid')


def calc_cm5(atoms, hirschfeld):  # TODO Test function.
    # all matrices have the naming scheme matrix[k,k'] according to the paper

    _radii = np.array([0.32, 0.37, 1.30, 0.99, 0.84, 0.75,
                       0.71, 0.64, 0.60, 0.62, 1.60, 1.40,
                       1.24, 1.14, 1.09, 1.04, 1.00, 1.01,
                       2.00, 1.74, 1.59, 1.48, 1.44, 1.30,
                       1.29, 1.24, 1.18, 1.17, 1.22, 1.20,
                       1.23, 1.20, 1.20, 1.18, 1.17, 1.16,
                       2.15, 1.90, 1.76, 1.64, 1.56, 1.46,
                       1.38, 1.36, 1.34, 1.30, 1.36, 1.40,
                       1.42, 1.40, 1.40, 1.37, 1.36, 1.36,
                       2.38, 2.06, 1.94, 1.84, 1.90, 1.88,
                       1.86, 1.85, 1.83, 1.82, 1.81, 1.80,
                       1.79, 1.77, 1.77, 1.78, 1.74, 1.64,
                       1.58, 1.50, 1.41, 1.36, 1.32, 1.30,
                       1.30, 1.32, 1.44, 1.45, 1.50, 1.42,
                       1.48, 1.46, 2.42, 2.11, 2.01, 1.90,
                       1.84, 1.83, 1.80, 1.80, 1.73, 1.68,
                       1.68, 1.68, 1.65, 1.67, 1.73, 1.76,
                       1.61, 1.57, 1.49, 1.43, 1.41, 1.34,
                       1.29, 1.28, 1.21, 1.22, 1.36, 1.43,
                       1.62, 1.75, 1.65, 1.57])
    _Dz = np.array([0.0056, -0.1543, 0.0000, 0.0333, -0.1030, -0.0446,
                    -0.1072, -0.0802, -0.0629, -0.1088, 0.0184, 0.0000,
                    -0.0726, -0.0790, -0.0756, -0.0565, -0.0444, -0.0767,
                    0.0130, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
                    0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
                    -0.0512, -0.0557, -0.0533, -0.0399, -0.0313, -0.0541,
                    0.0092, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
                    0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
                    -0.0361, -0.0393, -0.0376, -0.0281, -0.0220, -0.0381,
                    0.0065, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
                    0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
                    0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
                    0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
                    0.0000, 0.0000, -0.0255, -0.0277, -0.0265, -0.0198,
                    -0.0155, -0.0269, 0.0046, 0.0000, 0.0000, 0.0000,
                    0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
                    0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
                    0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
                    0.0000, 0.0000, 0.0000, 0.0000, -0.0179, -0.0195,
                    -0.0187, -0.0140, -0.0110, -0.0189])
    _alpha = 2.474
    _C = 0.705
    _DHC = 0.0502
    _DHN = 0.1747
    _DHO = 0.1671
    _DCN = 0.0556
    _DCO = 0.0234
    _DNO = -0.0346

    distances = atoms.get_all_distances(mic=True)
    atomic_numbers = np.array(atoms.numbers)
    Rz = _radii[atomic_numbers - 1]
    RzSum = np.tile(Rz, (len(Rz), 1))
    RzSum = np.add(RzSum, np.transpose(RzSum))
    Bkk = np.exp(-_alpha * (np.subtract(distances, RzSum)),
                 out=np.zeros_like(distances), where=distances != 0)
    assert (np.diagonal(Bkk) == 0).all()

    Dz = _Dz[atomic_numbers]
    #    Tkk = np.tile(Dz,(len(Dz),1))
    #    Tkk = np.subtract(Tkk, np.transpose(Tkk))
    Tkk = np.zeros(shape=Bkk.shape)
    shape = Tkk.shape
    for i in range(shape[0]):
        for j in range(shape[1]):
            numbers = [atomic_numbers[i], atomic_numbers[j]]
            if numbers[0] == numbers[1]:
                continue
            if set(numbers) == {1, 6}:
                Tkk[i, j] = _DHC
                if numbers == [6, 1]:
                    Tkk[i, j] *= -1.0
            elif set(numbers) == {1, 7}:
                Tkk[i, j] = _DHN
                if numbers == [7, 1]:
                    Tkk[i, j] *= -1.0
            elif set(numbers) == {1, 8}:
                Tkk[i, j] = _DHO
                if numbers == [8, 1]:
                    Tkk[i, j] *= -1.0
            elif set(numbers) == {6, 7}:
                Tkk[i, j] = _DCN
                if numbers == [7, 6]:
                    Tkk[i, j] *= -1.0
            elif set(numbers) == {6, 8}:
                Tkk[i, j] = _DCO
                if numbers == [8, 6]:
                    Tkk[i, j] *= -1.0
            elif set(numbers) == {7, 8}:
                Tkk[i, j] = _DNO
                if numbers == [8, 7]:
                    Tkk[i, j] *= -1.0
            else:
                Tkk[i, j] = _Dz[numbers[0] - 1] - _Dz[numbers[1] - 1]
    assert (np.diagonal(Tkk) == 0).all()
    product = np.multiply(Tkk, Bkk)
    assert (np.diagonal(product) == 0).all()
    result = np.sum(product, axis=1)
    return np.array(hirschfeld) + result


def generate_cm5_input(atoms, outcar_file, cm5_inp_file):
    """Writes an cm5pac input file from the data of OUTCAR and CONTCAR files.

    @param atoms: ase.Atoms object
    @param outcar_file: absolute path or filename of OUTCAR file.
    @param cm5_inp_file: absolute path or filename of cm5pac input file.
    """
    from modules.utilities import check_bak

    check_bak(cm5_inp_file)
    cm5_fh = open(cm5_inp_file, "w")

    # Write label and cell
    cm5_fh.write("\n")
    cm5_fh.write('%21.16f\n' % 1.0)
    for vec in atoms.cell:
        cm5_fh.write("  ")
        for coord in vec:
            cm5_fh.write('%21.16f ' % coord)
        cm5_fh.write("\n")

    # Write atomic numbers and coordinates
    cm5_fh.write('Direct\n')
    for atom in atoms:
        cm5_fh.write(str(atom.number))
        for coord in atom.scaled_position:
            cm5_fh.write('%20.16f' % coord)
        cm5_fh.write("\n")

    # Write charges
    hirsh_first_line = np.inf
    hirsh_last_line = -np.inf
    cm5_fh.write("----\n")
    with open(outcar_file) as outcar_fh:
        for lin_num, line in enumerate(outcar_fh):
            if line.lstrip().startswith("Hirshfeld-DOM"):
                hirsh_charges = []
                hirsh_first_line = lin_num + 3
                hirsh_last_line = len(atoms) + lin_num + 2
            elif hirsh_first_line <= lin_num <= hirsh_last_line:
                hirsh_charges.append(line.split()[3])
    for charge in hirsh_charges:
        cm5_fh.write(charge + "\n")
    cm5_fh.close()


def read_cm5_charges(cm5_out_file, neutral=True):
    """Reads CM5 charges from a CM5pac output file.

    @param cm5_out_file: The CM5pac output file.
    @param neutral: Whether the system should be neutral or not.
    @return: cm5 charges
    """
    cm5_first_line = np.inf
    cm5_charges = []
    with open(cm5_out_file) as cm5out_fh:
        for lin_num, line in enumerate(cm5out_fh):
            if "Hirshfeld" in line:
                cm5_first_line = lin_num + 3
            elif lin_num >= cm5_first_line and "--" not in line:
                cm5_charges.append(float(line.split()[2]))
    if neutral:
        tot_chg = np.sum(cm5_charges)
        avg_chg_deviation = tot_chg / len(cm5_charges)
        return [chg - avg_chg_deviation for chg in cm5_charges]
    else:
        return cm5_charges


def compute_charges(atoms, outcar_file, target_dir, cm5pac_dir):
    """Assigns CM5 charges from OUTCAR's Hirshfeld charges into an Atoms object.

    @param atoms: ase.Atoms object
    @param outcar_file: The OUTCAR file produced by VASP.
    @param target_dir: The intermediate directory for CM5 input/output.
    @param cm5pac_dir: The path to the directory where cm5pac.exe is.
    @return: ase.Atoms object with CM5 charges.
    """
    from subprocess import Popen
    cm5_in_file = target_dir + "/cm5inp"
    cm5_out_file = target_dir + "/cm5out"
    generate_cm5_input(atoms, outcar_file, cm5_in_file)
    cmd = f"{cm5pac_dir}/cm5pac.exe 3 < {cm5_in_file} > {cm5_out_file}"
    Popen(cmd, shell=True).wait()
    charges = [23.142321 * chg for chg in read_cm5_charges(cm5_out_file)]
    if len(charges) != len(atoms):
        err_msg = f"There's a mismatch between number of atoms ({len(atoms)}) " \
                  f"and charges ({len(charges)})."
        logger.error(err_msg)
        raise ValueError(err_msg)
    for atom, charge in zip(atoms, charges):
        atom.charge = charge
    return atoms


def write_charged_prmtop(charges, unchg_prmtop, chg_prmtop):
    """Writes a prmtop file with the given charges and an uncharged prmtop file.

    @param charges: List of charges
    @param unchg_prmtop: The uncharged prmtop file to read.
    @param chg_prmtop: The charged prmtop file to write.
    """
    unchg_prmtop_fh = open(unchg_prmtop)
    chg_prmtop_fh = open(chg_prmtop, "w")
    charges_first_line = np.inf
    charges_last_line = -np.inf
    for lin_num, line in enumerate(unchg_prmtop_fh):
        # Search for the charges section:
        if "%FLAG CHARGE" in line:
            charges_first_line = lin_num + 2
            charges_last_line = charges_first_line + np.ceil(len(charges)/5) - 1
            chg_prmtop_fh.write(line)
        # Write charges (5 per line):
        elif charges_first_line <= lin_num <= charges_last_line:
            rel_lin_num = lin_num - charges_first_line
            low_bound = rel_lin_num * 5
            high_bound = rel_lin_num * 5 + 5
            for charge in charges[low_bound:high_bound]:
                chg_prmtop_fh.write(" {:15.8E}".format(charge))
            # When the charges section in the uncharged prmtop file has more
            # values than the 'charges' list (eg. solvated), the last line
            # written with the values of 'charges' needs to be completed with
            # the charges in the uncharged prmtop file:
            if lin_num == charges_last_line and len(line.split()) \
                    > len(charges[low_bound:high_bound]):
                for el in line.split()[len(charges[low_bound:high_bound]):]:
                    chg_prmtop_fh.write(" {:15.8E}".format(float(el)))
            chg_prmtop_fh.write("\n")
        else:
            chg_prmtop_fh.write(line)
    unchg_prmtop_fh.close()
    chg_prmtop_fh.close()

"""Functions to read SolvHybrid input file.

List of functions:

Auxiliary functions
-------------------
check_expect_val: Checks whether a keyword is assigned an expected value.
str2lst: Converts a string of integers, and groups of them, to a list of lists.

Functions to read parameters in the Global section
--------------------------------------------------
get_solvation_width: Gets 'solvation_width' value and checks that its value is
    acceptable.
get_run_type: Gets 'run_type' value, checks it and returns the kind of execution
    to carry out

Functions to read parameters in the Surface section
---------------------------------------------------
get_ads_coord_file: Gets 'adsorbed_surf_fil' value and checks it's adequate.
get_ads_out_file(): Gets 'ads_out_file' value and checks it's adequate.
get_scale_factor: Gets 'scale_factor' value and checks it's adequate.
get_slab_coord_file: Gets 'slab_coord_file' value and checks it's adequate.
get_slab_out_file:  Gets 'ads_out_file' value and checks it's adequate.


read_input: Directs the reading of the parameters in the input file
"""
import os
import logging
from configparser import ConfigParser, NoSectionError, NoOptionError, \
    MissingSectionHeaderError, DuplicateOptionError

import numpy as np
import ase.io

from modules.utilities import try_command

logger = logging.getLogger('SolvHybrid')

inp_parser = ConfigParser(inline_comment_prefixes='#',
                          empty_lines_in_values=False)

# Define new answers to be interpreted as True or False.
new_answers = {'n': False, 'none': False, 'nay': False,
               'y': True, 'sí': True, 'aye': True, 'sure': True}
for answer, val in new_answers.items():
    inp_parser.BOOLEAN_STATES[answer] = val
turn_false_answers = [answer for answer in inp_parser.BOOLEAN_STATES
                      if inp_parser.BOOLEAN_STATES[answer] is False]
turn_true_answers = [answer for answer in inp_parser.BOOLEAN_STATES
                     if inp_parser.BOOLEAN_STATES[answer]]

# Template error messages to be customized in place.
no_sect_err = "Section '%s' not found on input file"
no_opt_err = "Option '%s' not found on section '%s'"
num_error = "'%s' value must be a %s"


# Auxilary functions (sorted alphabetically)

def check_expect_val(value, expect_vals, err_msg=None):
    """Checks whether a keyword is assigned an expected value (value list).

    @param value: The value to be checked as expected or not.
    @param expect_vals: list of values acceptable for the given keyword.
    @param err_msg: The error message to be prompted in both log and screen.
    @raise ValueError: if the value is not among the expected ones.
    @return True if the value is among the expected ones.
    """
    if err_msg is None:
        err_msg = f"'{value}' is not an adequate value.\n" \
                  f"Adequate values: {expect_vals}"
    if value not in expect_vals:
        logger.error(err_msg)
        raise ValueError(err_msg)

    return True


def str2lst(cmplx_str, func=int):  # TODO: enable deeper level of nested lists
    # TODO Treat all-enclosing parenthesis as a list instead of list of lists.
    """Converts a string of integers/floats, and groups of them, to a list.

    Keyword arguments:
    @param cmplx_str: str, string of integers (or floats) and groups of them
    enclosed by parentheses-like characters.
    - Group enclosers: '()' '[]' and '{}'.
    - Separators: ',' ';' and ' '.
    - Nested groups are not allowed: '3 ((6 7) 8) 4'.
    @param func: either to use int or float

    @return list, list of integers (or floats), or list of integers (or floats)
    in the case they were grouped. First, the singlets are placed, and then the
    groups in input order.

    eg. '128,(135 138;141] 87 {45, 68}' -> [128, 87, [135, 138, 141], [45, 68]]
    """

    # Checks
    error_msg = "Function argument should be a str, sequence of " \
                "numbers separated by ',' ';' or ' '." \
                "\nThey can be grouped in parentheses-like enclosers: '()', " \
                "'[]' or {}. Nested groups are not allowed. \n" \
                "eg. 128,(135 138;141) 87 {45, 68}"
    cmplx_str = try_command(cmplx_str.replace, [(AttributeError, error_msg)],
                            ',', ' ')

    cmplx_str = cmplx_str.replace(';', ' ').replace('[', '(').replace(
        ']', ')').replace('{', '(').replace('}', ')')

    try_command(list, [(ValueError, error_msg)], map(func, cmplx_str.replace(
        ')', '').replace('(', '').split()))

    depth = 0
    for el in cmplx_str.split():
        if '(' in el:
            depth += 1
        if ')' in el:
            depth += -1
        if depth > 1 or depth < 0:
            logger.error(error_msg)
            raise ValueError(error_msg)

    init_list = cmplx_str.split()
    start_group = []
    end_group = []
    for i, element in enumerate(init_list):
        if '(' in element:
            start_group.append(i)
            init_list[i] = element.replace('(', '')
        if ')' in element:
            end_group.append(i)
            init_list[i] = element.replace(')', '')

    init_list = list(map(func, init_list))

    new_list = []
    for start_el, end_el in zip(start_group, end_group):
        new_list.append(init_list[start_el:end_el + 1])

    for v in new_list:
        for el in v:
            init_list.remove(el)
    return init_list + new_list


# Global (sorted alphabetically)
def get_cm5pac_dir():
    """Gets 'cm5pac_dir' value and checks that its value is acceptable.

    @return cm5pac_dir: The directory containing cm5pac executable.
    """
    cm5pac_dir = inp_parser.get('Global', 'cm5pac_dir')
    if not os.path.isdir(cm5pac_dir):
        err_msg = f"Provided directory {cm5pac_dir} not found"
        logger.error(err_msg)
        raise NotADirectoryError(err_msg)
    elif 'cm5pac.exe' not in os.listdir(cm5pac_dir):
        err_msg = f"'cm5pac' not found on {cm5pac_dir}"
        logger.error(err_msg)
        raise FileNotFoundError(err_msg)
    return cm5pac_dir


def get_solvation_width():
    """Gets 'solvation_thickness' value and checks that its value is acceptable.

    @return solvation_thickness: The thickness of solvent around the system (Å).
    """
    solvation_thickness = inp_parser.getint('Global', 'solvation_thickness',
                                            fallback=30)
    if solvation_thickness <= 0:
        err_msg = num_error % ('solvation_thickness', 'positive integer')
        logger.error(err_msg)
        raise ValueError(err_msg)
    return solvation_thickness


def get_run_type():
    """Gets 'run_type' value, checks it and returns the kind of execution to
    carry out.
    """
    surface, bulk = (False, False)
    run_type_vals = ["surface", "bulk"]
    run_types_inp_str = inp_parser.get('Global', 'run_type').lower()
    run_types_inp_list = str2lst(run_types_inp_str, str)
    for value in run_types_inp_list:
        if isinstance(value, list):
            err_msg = "'run_type' does not accept nested lists of values."
            logger.error(err_msg)
            raise ValueError(err_msg)
        check_expect_val(value, run_type_vals)
        if value == "surface":
            surface = True
        if value == "bulk":
            bulk = True
    return surface, bulk


# Surface
def get_ads_coord_file():
    """Gets 'ads_coord_file' value and checks that its value is acceptable.

    @return ads_coord_file: The file with the adsorbed surface coordinates
    """
    ads_coord_file = inp_parser.get('Surface', 'ads_coord_file')
    if not os.path.isfile(ads_coord_file):
        logger.error(f'File {ads_coord_file} not found.')
        raise FileNotFoundError(f'File {ads_coord_file} not found')
    err_msg = f"File '{ads_coord_file}' is not supported."
    try_command(ase.io.read, [(Exception, err_msg)], ads_coord_file)
    return ads_coord_file


def get_ads_out_file():
    """Gets 'ads_out_file' value and checks that its value is acceptable.

    @return ads_out_file: The output file of the adsorbed surface calculation
    """
    ads_out_file = inp_parser.get('Surface', 'ads_out_file')
    if not os.path.isfile(ads_out_file):
        logger.error(f'File {ads_out_file} not found.')
        raise FileNotFoundError(f'File {ads_out_file} not found')
    return ads_out_file


def get_pbc_cell():
    """Gets 'pbc_cell' value and checks that its value is acceptable.

    :return: pbc_cell The cell for the periodic boundary conditions.
    """
    from ase.atoms import Cell
    err_msg = "'pbc_cell' must be either 3 vectors of size 3 or False."
    pbc_cell_str = inp_parser.get('Surface', 'pbc_cell', fallback="false")
    if pbc_cell_str.lower() in turn_false_answers:
        return False
    else:
        pbc_cell = Cell(try_command(str2lst, [(ValueError, err_msg)],
                                    pbc_cell_str, float))
        if pbc_cell.shape != (3, 3):
            logger.error(err_msg)
            raise ValueError(err_msg)
        if np.linalg.det(pbc_cell) == 0.0:
            err_msg = "The volume of the defined cell is 0"
            logger.error(err_msg)
            raise ValueError(err_msg)
        return pbc_cell


def get_scale_factor():
    """Gets 'scale_factor' value and checks that its value is acceptable.

        :return: scale_factor: The number of times to multiply both the pristine
            surface and the adsorbed surface in x and y dimensions.
        """
    scale_factor = inp_parser.getint("Surface", "scale_factor", fallback=2)
    if scale_factor <= 0:
        err_msg = num_error % ("scale_factor", "positive integer")
        logger.error(err_msg)
        raise ValueError(err_msg)

    return scale_factor


def get_slab_coord_file():
    """Gets 'slab_coord_file' value and checks that its value is acceptable.

    :return: slab_coord_file: The file with the pristine surface coordinates
    """
    slab_coord_file = inp_parser.get('Surface', 'slab_coord_file')
    if not os.path.isfile(slab_coord_file):
        logger.error(f'File {slab_coord_file} not found.')
        raise FileNotFoundError(f'File {slab_coord_file} not found')
    err_msg = f"File '{slab_coord_file}' is not supported."
    try_command(ase.io.read, [(Exception, err_msg)], slab_coord_file)
    return slab_coord_file


def get_slab_out_file():
    """Gets 'slab_out_file' value and checks that its value is acceptable.

    :return: slab_out_file: The output file of the pristine surface calculation
    """
    slab_out_file = inp_parser.get('Surface', 'slab_out_file')
    if not os.path.isfile(slab_out_file):
        logger.error(f'File {slab_out_file} not found.')
        raise FileNotFoundError(f'File {slab_out_file} not found')
    return slab_out_file


# Bulk

# Read input parameters

def read_input(in_file):
    """Directs the reading of the parameters in the input file.

    @param in_file: The path to the DockOnSurf input file.
    @return inp_vars: Dictionary with the values for every option in the input
        file.
    """

    # Checks for errors in the Input file.
    err_msg = False
    try:
        inp_parser.read(in_file)
    except MissingSectionHeaderError as e:
        logger.error('There are options in the input file without a Section '
                     'header.')
        err_msg = e
    except DuplicateOptionError as e:
        logger.error('There is an option in the input file that has been '
                     'specified more than once.')
        err_msg = e
    except Exception as e:
        err_msg = e
    else:
        err_msg = False
    finally:
        if isinstance(err_msg, BaseException):
            raise err_msg

    inp_vars = {}

    # Global
    if not inp_parser.has_section('Global'):
        logger.error(no_sect_err % 'Global')
        raise NoSectionError('Global')

    # Mandatory keywords
    # Checks whether the mandatory keywords are present.
    glob_mand_opts = ['run_type', 'cm5pac_dir']
    for opt in glob_mand_opts:
        if not inp_parser.has_option('Global', opt):
            logger.error(no_opt_err % (opt, 'Global'))
            raise NoOptionError(opt, 'Global')

    inp_vars['surface'], inp_vars['bulk'] = get_run_type()
    inp_vars['cm5pac_dir'] = get_cm5pac_dir()

    # Optional keywords
    inp_vars['solvation_width'] = get_solvation_width()

    # Surface
    if inp_vars["surface"]:
        if not inp_parser.has_section('Surface'):
            logger.error(no_sect_err % 'Surface')
            raise NoSectionError('Surface')
        # Mandatory options:
        # Checks whether the mandatory options are present.
        surf_mand_kywds = ["scale_factor"]
        for opt in surf_mand_kywds:
            if not inp_parser.has_option('Surface', opt):
                logger.error(no_opt_err % (opt, 'Surface'))
                raise NoOptionError(opt, 'Surface')

        inp_vars["scale_factor"] = get_scale_factor()
        inp_vars["slab_coord_file"] = get_slab_coord_file()
        inp_vars["slab_out_file"] = get_slab_out_file()
        inp_vars["ads_coord_file"] = get_ads_coord_file()
        inp_vars["ads_out_file"] = get_ads_out_file()
        inp_vars['pbc_cell'] = get_pbc_cell()

        surfs = [ase.io.read(inp_vars["slab_coord_file"]),
                 ase.io.read(inp_vars["ads_coord_file"])]

        if inp_vars['pbc_cell'] is False:
            if not np.allclose(surfs[0].cell, surfs[1].cell):
                err_msg = "'slab_coord_file' and 'ads_coord_file' have" \
                          "different cells, check their values or provide a " \
                          "new one with 'pbc_cell' keyword."
                logger.error(err_msg)
                raise ValueError(err_msg)
            if np.linalg.det(surfs[0].cell) == 0.0:
                err_msg = "When running 'Surface' calculations the PBC cell " \
                          "should be provided either implicitely inside " \
                          "'slab_coord_file'/'ads_coord_file' or using " \
                          "the 'pbc_cell' option."
                logger.error(err_msg)
                raise ValueError(err_msg)
            inp_vars['pbc_cell'] = surfs[0].cell
            logger.info("Obtained 'pbc_cell' from 'slab_coord_file' file.")
        elif np.allclose(surfs[0].cell, surfs[1].cell) \
                and np.linalg.det(surfs[0].cell) != 0 \
                and not np.allclose(inp_vars['pbc_cell'], surfs[0].cell):
            logger.warning("'slab_coord_file' and 'ads_coord_file' have "
                           "an implicit cell defined different than "
                           "'pbc_cell' keyword.\n"
                           f"'slab_coord_file'/'ads_coord_file' = "
                           f"{surfs[0].cell}.\n"
                           f"'pbc_cell' = {inp_vars['pbc_cell']}).\n"
                           "'pbc_cell' value will be used.")

    return_vars_str = "\n\t".join([str(key) + ": " + str(value)
                                   for key, value in inp_vars.items()])
    logger.info(f'Correctly read {in_file} parameters:'
                f' \n\n\t{return_vars_str}\n')

    return inp_vars

import logging

logger = logging.getLogger('SolvHybrid')


def scale_xy(atoms, scale_factor):
    """Replicates the atoms object along the X and Y axis a number of times.

    @param atoms: The ase.Atoms object to replicate.
    @param scale_factor: The number of times to replicate the atoms object.
    @return: the ase.Atoms object once replicated in the X and Y dimensions.
    """
    import numpy as np
    from ase.build import make_supercell
    scale_matrix = np.identity(3) * (scale_factor, scale_factor, 1)
    return make_supercell(atoms, scale_matrix)


def build_dir_hierarchy():
    """Builds the directory hierarchy to work on.
    """
    import os
    from modules.utilities import check_bak
    # Pristine surface
    check_bak("surface")
    os.mkdir("surface")
    os.mkdir("surface/vacuum")
    os.mkdir("surface/solvent")

    # Adsorbed surface
    check_bak("adsorbed")
    os.mkdir("adsorbed")
    os.mkdir("adsorbed/vacuum")
    os.mkdir("adsorbed/solvent")

    # tleap inps and logs
    check_bak("tleap")
    os.mkdir("tleap")


def run_surface(inp_vars):
    """Carries out the procedures for the surface part of the calculations.

    @param inp_vars: A dictionary with all the variables read from the input.
    @return:
    """
    import numpy as np
    import ase.io
    from modules.formats import save_pdb, save_amber_parms, set_proper_masses, \
        read_pdb
    from modules.solvation import solvate_tleap, cut_solvation_box, \
        copy_solvent
    from modules.charges import compute_charges, write_charged_prmtop
    from modules.calculation import minimize

    # Read files and extract cells if necessary
    prist_surf = ase.io.read(inp_vars['slab_coord_file'])
    if not np.allclose(inp_vars['pbc_cell'], prist_surf.cell):
        prist_surf.cell = inp_vars['pbc_cell']
    ads_surf = ase.io.read(inp_vars['ads_coord_file'])
    if not np.allclose(inp_vars['pbc_cell'], ads_surf.cell):
        ads_surf.cell = inp_vars['pbc_cell']

    build_dir_hierarchy()

    # Compute charges
    prist_surf = compute_charges(prist_surf, inp_vars["slab_out_file"],
                                 "surface", inp_vars['cm5pac_dir'])
    ads_surf = compute_charges(ads_surf, inp_vars["ads_out_file"],
                               "adsorbed", inp_vars['cm5pac_dir'])

    # Extend surface in x and y dimensions
    scaled_prist_surf = scale_xy(prist_surf, inp_vars["scale_factor"])
    scaled_ads = scale_xy(ads_surf, inp_vars["scale_factor"])
    logger.info("Scaled pristine and adsorbed surfaces.")

    # Convert files to Amber-compatible PDBs and save amber parameters.
    save_pdb(scaled_prist_surf, "surface/scaled_surf.pdb")
    save_pdb(scaled_ads, "adsorbed/scaled_ads.pdb")
    save_amber_parms("adsorbed/scaled_ads.pdb", "adsorbed/vacuum")
    save_amber_parms("surface/scaled_surf.pdb", "surface/vacuum")

    # Write charges in the prmtop file.
    write_charged_prmtop(scaled_prist_surf.get_initial_charges(),
                         "surface/vacuum/uncharged_prmtop",
                         "surface/vacuum/prmtop")
    write_charged_prmtop(scaled_ads.get_initial_charges(),
                         "adsorbed/vacuum/uncharged_prmtop",
                         "adsorbed/vacuum/prmtop")

    # Perform solvation
    solvate_tleap("adsorbed/scaled_ads.pdb", "adsorbed/solv_ads_rect.pdb",
                  inp_vars['solvation_width'])
    solv_ads = cut_solvation_box("adsorbed/solv_ads_rect.pdb", scaled_ads)

    # Rotate the system 90º around the y axis.
    spec_atms = [("H1", "H"), ("H2", "H")]
    solv_ads = set_proper_masses(solv_ads, spec_atms)
    solv_ads.rotate(90, [0, 1, 0], solv_ads.get_center_of_mass(),
                    rotate_cell=True)
    scaled_prist_surf.rotate(90, [0, 1, 0],
                             scaled_prist_surf.get_center_of_mass())
    logger.info("Rotated pristine and adsorbed surfaces to avoid problems with"
                " frozen atoms in amber.")
    save_pdb(solv_ads, "adsorbed/rot_solv_ads.pdb")

    # Save amber parameters for the solvated adsorbed structure.
    save_amber_parms("adsorbed/rot_solv_ads.pdb", "adsorbed/solvent",
                     write_box=True)
    write_charged_prmtop(scaled_ads.get_initial_charges(),
                         "adsorbed/solvent/uncharged_prmtop",
                         "adsorbed/solvent/prmtop")

    # Minimize Geometry of the solvated adsorbed configuration and copy its
    # solvation box to the pristine surface.
    minimize("adsorbed/solvent")
    solv_ads_min = read_pdb("adsorbed/solvent/minimized.pdb", spec_atms)
    solv_surf = copy_solvent(solv_ads_min, scaled_prist_surf)

import logging
import ase

logger = logging.getLogger('SolvHybrid')


def solvate_tleap(init_file: str, solv_file: str, thickness: int):
    """Creates a solvent box around the given system using tleap.

    @param init_file: The pdb file of the given system.
    @param solv_file:
    @param thickness: The thickness of solvent around the system (Å).
    """
    import os.path
    from subprocess import Popen
    from ase.io.formats import filetype
    from modules.utilities import check_bak

    # File checks
    if not isinstance(init_file, str):
        err_msg = "'pdb_files' must be a string containing the file path."
        logger.error(err_msg)
        raise ValueError(err_msg)
    if not os.path.isfile(init_file):
        err_msg = f"File {init_file} not found."
        logger.error(err_msg)
        raise FileNotFoundError(err_msg)
    if not filetype(init_file) == 'proteindatabank':
        err_msg = "At least one of the files is not a pdb file."
        logger.error(err_msg)
        raise ValueError(err_msg)

    # Create tleap input file
    modules_dir = os.path.dirname(__file__)
    tleap_inp = "loadoff tip3pbox.off\n" \
                f"source {modules_dir}/leaprc.uff\n" \
                f"loadamberparams {modules_dir}/uff.dat\n" \
                f"loadamberparams {modules_dir}/music.dat\n" \
                f"loadoff {modules_dir}/uff.lib\n" \
                f"adsorba = loadpdb {init_file}\n" \
                f"solvatebox adsorba TIP3PBOX {thickness}\n" \
                f"savepdb adsorba {solv_file}\n" \
                f"quit\n"
    check_bak("tleap/tleap-hyd.inp")
    with open("tleap/tleap-hyd.inp", "w") as tleap_fh:
        tleap_fh.write(tleap_inp)

    # Execute tleap
    with open("tleap/tleap-hyd.log", "w") as tleap_log:
        Popen("tleap -f tleap/tleap-hyd.inp", shell=True, stdout=tleap_log,
              stderr=tleap_log).wait()

    logger.info("Created solvation layer (cuboid) around the adsorbed surface.")


def point_in_cell(p, cell, translate=False):
    """Checks whether a given point lies inside a given cell.

    This function checks whether a given point defined by [x,y,z] lies inside a
    given cell defined by [a,b,c] where a=[xa,ya,za], b=[xb,yb,zb],
    c=[xc,yc,zc]. If not and translate=True it translates the point inside the
    cell by adding integer times the cell vectors, this way preserving its
    relative position respect the cell copy. It applies Periodic Boundary
    Conditions.
    The check is performed by considering that a point p lies inside the cell,
    when it can be expressed as:
    p = alpha * a + beta * b + gamma * c,   with:  0 < alpha, beta, gamma < 1.
    alpha, beta and gamma are calculated as the determinant of the matrix
    defined by substituting the cell component whose coefficient we want to
    calculate by p, everything divided by the cell volume (det[a,b,c])
    e.g: alpha = det([p,b,c])/det([a,b,c]); beta = det([a,p,c])/det([a,b,c])
    gamma = det([a,b,p])/det([a,b,c])

    @param p: The cartesian coordinates of the point.
    @param cell: The h matrix composing the cell: The a, b, and c cell vectors.
    @param translate: Whether to translate the point inside the cell respecting
        its relative position respect to the periodic copy of the cell that lies
        within.

    @return: True or False, if 'translate' is 'False'. If 'translate' is 'True'
        it returns 'p_prime': The new position inside the cell after applying a
        translation consisting of an integer number of times the cell vectors
        ie. the position of 'p' respect the corresponding periodic copy of
        the cell.
    """
    from collections import Sized, Iterable
    import numpy as np
    from modules.utilities import try_command

    # Check function parameters
    if not isinstance(p, (Iterable, Sized)) or isinstance(p, (str, dict)):
        err_msg = "'p' is not an array-like variable"
        logger.error(err_msg)
        raise TypeError(err_msg)
    if not isinstance(cell, (Iterable, Sized)) or isinstance(cell, (str, dict)):
        err_msg = "'cell' is not an array-like variable"
        logger.error(err_msg)
        raise TypeError(err_msg)
    if not isinstance(p, np.ndarray):
        p = np.array(p)
    if not isinstance(cell, np.ndarray):
        cell = np.array(cell)
    if p.shape != (3,):
        err_msg = "'p' must be a vector of size 3."
        logger.error(err_msg)
        raise ValueError(err_msg)
    if cell.shape != (3, 3):
        err_msg = "'cell' must be a 3x3 matrix."
        logger.error(err_msg)
        raise ValueError(err_msg)
    err_msg = "The elements of 'p' must be numbers."
    [try_command(float, [(ValueError, err_msg)], el) for el in p]
    err_msg = "The elements of 'cell' must be numbers."
    [try_command(float, [(ValueError, err_msg)], el) for el in cell.flatten()]

    # Compute the cell volume and the alpha, beta and gamma factors.
    volume = np.linalg.det(cell)

    alpha = np.linalg.det(np.array([p, cell[1], cell[2]])) / volume
    beta = np.linalg.det(np.array([cell[0], p, cell[2]])) / volume
    gamma = np.linalg.det(np.array([cell[0], cell[1], p])) / volume

    if not all([0 < value < 1 for value in [alpha, beta, gamma]]):
        in_cell = False
    else:
        in_cell = True

    if not translate:
        return in_cell
    else:
        p_prime = np.array(p)
        if alpha < 0:
            p_prime = p_prime + (1 + int(-alpha)) * cell[0]
        elif alpha > 1:
            p_prime = p_prime + int(-alpha) * cell[0]
        if beta < 0:
            p_prime = p_prime + (1 + int(-beta)) * cell[1]
        elif beta > 1:
            p_prime = p_prime + int(-beta) * cell[1]
        if gamma < 0:
            p_prime = p_prime + (1 + int(-gamma)) * cell[2]
        elif gamma > 1:
            p_prime = p_prime + int(-gamma) * cell[2]
        return p_prime


def cut_solvation_box(rect_solv_pdb, scaled_ads):
    """Deletes the solvation molecules that lie outside the working cell.

    @param rect_solv_pdb: The pdb file generated by leap.
    @param scaled_ads: ase.Atoms object of the non-solvated scaled adsorbate.
    @return: ase.Atoms object of the solvated system within the working cell
    """
    # TODO Parallelize
    from modules.formats import read_pdb

    # Read the solvated adsorbate (cuboid)
    rect_solv_ads = read_pdb(rect_solv_pdb, [("H1", "H"), ("H2", "H")])

    # Define the cell of the solvated adsorbate as the cell of the scaled
    # adsorbate with the height of the cuboid solvated cell generated by leap.
    cell_solv = scaled_ads.cell
    cell_solv[2, 2] = rect_solv_ads.cell[2, 2]
    rect_solv_ads.cell = cell_solv
    rect_solv_ads.center()

    # Check which atoms lie outside the cell to delete them afterwards.
    del_tags = []
    for atom in rect_solv_ads:
        if atom.index < len(scaled_ads) or atom.tag in del_tags:
            continue
        if not point_in_cell(atom.position, cell_solv):
            del_tags.append(atom.tag)

    # Delete the atoms
    del rect_solv_ads[[atom.index for atom in rect_solv_ads
                       if atom.tag in del_tags]]
    logger.info("Re-shaped solvation box to fit simulation cell.")
    return rect_solv_ads


def copy_solvent(solvated_atoms, target_atoms, solv="WAT"):
    """Copies the solvation box from an atoms object to another atoms object.

    @param solvated_atoms: ase.Atoms object containing the solvation box.
    @param target_atoms: ase.Atoms object without solvent.
    @param solv:
    @return: ase.Atoms object of the target_atoms with the solvation box.
    """
    solvent = solvated_atoms[[atom.index for atom in solvated_atoms
                              if atom.tag in solvated_atoms.info[solv]]]
    return target_atoms.extend(solvent)
